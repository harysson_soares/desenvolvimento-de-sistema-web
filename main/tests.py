import datetime

from django.test import TestCase
from django.utils import timezone
from django.test.utils import setup_test_environment
from django.urls import reverse

from .models import Question

# Create your tests here.

class QuestionTest(TestCase):
  
  def test_metodo_publicada_recentemente_com_pergunta_no_futuro(self):
    """
      O método publicada recentemente precisa retornar false quando se tratar de perguntas com data
      de publicação no futuro.
    """
    data = timezone.now() + datetime.timedelta(seconds=1)
    pergunta = Question(data_publicacao = data)

    self.assertIs(pergunta.publicada_recentemente(), False)

  def test_metodo_publicada_recentemente_com_data_anterior_a_24h_no(self):

    """
    O metodo publicada recentemente deve retornar false quando se tratar de uma data de publicaçao anterior a 24 horas do passado
    """
    data = timezone.now() - datetime.timedelta(days = 1, seconds=1)
    pergunta = Question(data_publicacao = data)
    
    self.assertIs(pergunta.publicada_recentemente(), False)

    
  def test_metodo_publicada_recentemente_com_data_nas_utlimas_24h(self):

    """
    O metodo publicada recentemente deve retornar true quando se tratar de uma data de publicaçao dentro das ultimas 24 horas
    """
    data = timezone.now() - datetime.timedelta(hours= 23, minutes=59, seconds=59)
    pergunta = Question(data_publicacao = data)
    
    self.assertIs(pergunta.publicada_recentemente(), True)


def criar_pergunta(text, dias):
  """
    Função para criação de uma pergunta para texto e uam variação de dias 
  """

  data = timezone.now() + datetime.timedelta(days=dias)

  return Question.objects.create(text=text, data_publicacao=data)

class IndexViewTest(TestCase):

  def test_sem_perguntas_cadastradas(self):
    """
      Teste da Index view quando não houver perguntas cadastradas.
      exibida uma messagem específica
    """
    resposta = self.client.get(reverse("main:index"))
    self.assertEqual(resposta.status_code, 200)
    self.assertContains(resposta, "Não há enquetes cadastradas até o momento!")
    self.assertQuerysetEqual(resposta.context["ultimas_perguntas"], [])

  def test_com_pergunta_no_passado(self):
    """
      Teste da Index view exibindo normalmente pergunta no passado
    """

    criar_pergunta(text="Pergunta no passado", dias=-30)
    resposta = self.client.get(reverse("main:index"))
    self.assertEqual(resposta.status_code, 200)
    # self.assertContains(resposta, "Não há enquetes cadastradas até o momento!")
    self.assertQuerysetEqual(resposta.context["ultimas_perguntas"], ['<Question: Pergunta no passado>'])
  
  def test_com_pergunta_no_futuro(self):
    """
      Perguntas Com data de publicação no futuro não devem ser exibidas
    """

    criar_pergunta(text="Pergunta no futuro", dias=1)
    resposta = self.client.get(reverse("main:index"))
    self.assertEqual(resposta.status_code, 200)
    self.assertContains(resposta, "Não há enquetes cadastradas até o momento!")
    self.assertQuerysetEqual(resposta.context["ultimas_perguntas"], [])
  
  def test_com_pergunta_no_passado_e_no_futuro(self):
    """
      Perguntas no passado são exibidas e no futuro são omitidas
    """

    criar_pergunta(text="Pergunta no futuro", dias=1)
    criar_pergunta(text="Pergunta no passado", dias=-1)
    resposta = self.client.get(reverse("main:index"))
    self.assertEqual(resposta.status_code, 200)
    self.assertContains(resposta, "Pergunta no passado")
    self.assertQuerysetEqual(resposta.context["ultimas_perguntas"], ['<Question: Pergunta no passado>'])

  def test_com_duas_pergunta_no_passado(self):
    """
      Teste da Index view exibindo duas perguntas no passado
    """

    criar_pergunta(text="Pergunta no passado 1", dias=-1)
    criar_pergunta(text="Pergunta no passado 2", dias=-5)
    resposta = self.client.get(reverse("main:index"))
    self.assertEqual(resposta.status_code, 200)
    # self.assertContains(resposta, "Não há enquetes cadastradas até o momento!")
    self.assertQuerysetEqual(resposta.context["ultimas_perguntas"], ['<Question: Pergunta no passado 1>', '<Question: Pergunta no passado 2>'])

class DetailsViewTest(TestCase):

  def test_pergunta_no_futuro(self):
    """
      Deverá retornar um error 404 ao indicar uma pergunta com data no futuro
    """

    pergunta = criar_pergunta(text = "Pergunta no Futuro", dias = 1)
    resposta = self.client.get(reverse("main:detalhes", args=[pergunta.id]))
    self.assertEqual(resposta.status_code, 404)

  def test_pergunta_no_passado(self):
    """
      Deverá retornar um 200 ao indicar uma pergunta com data no passado
    """

    pergunta = criar_pergunta(text = "Pergunta no passado", dias = -1)
    resposta = self.client.get(reverse("main:detalhes", args=[pergunta.id]))
    self.assertEqual(resposta.status_code, 200)
    self.assertContains(resposta, pergunta.text)