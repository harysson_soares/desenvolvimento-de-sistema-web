from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=500)
    started_at = models.DateTimeField('Data de criação')

    class Meta:
        ordering = ['title']
    
    def __str__(self):
        return self.title

class Question(models.Model):
    text = models.CharField(max_length=200)
    data_publicacao = models.DateTimeField('Data de Publicação')
    category = models.ManyToManyField(Category)

    def __str__(self):
        return self.text

    class Meta:
        ordering = ['text']

    def publicada_recentemente(self):
        agora = timezone.now()
        return agora - datetime.timedelta(days=1) <= self.data_publicacao <= agora

    publicada_recentemente.admin_order_field = 'data_publicacao'
    publicada_recentemente.boolean = True
    publicada_recentemente.short_description = 'RECENTEMENTE PUBLICADA? (<24H)'

class Polls(models.Model):
    title = models.CharField(max_length=200)
    create_at = models.DateTimeField('Data de criação')
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
    
class Choice(models.Model):
    text = models.CharField(max_length=200)
    votos = models.IntegerField(default=0)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.text