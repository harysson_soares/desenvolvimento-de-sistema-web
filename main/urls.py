from django.urls import path
from . import views

app_name = 'main'

urlpatterns= [
    path('', views.IndexView.as_view(), name='index'),
    # path('enquente/<int:pk>', views.DetalhesView.as_view(), name='detalhes'),
    path('enquente/<int:pk>/resultado', views.ResultadoView.as_view(), name='resultado'),
    path('enquente/<int:id_enquete>/votacao', views.votacao, name='votacao'),
    path('enquente/<int:id_enquete>', views.detalhes, name='detalhes'),
    path('enquente/categoria/<int:id_category>', views.categories, name='categorias'),
    #path('enquente/<int:id_enquete>/resultado', views.ResultadoView.as_view(), name='resultado'),
    #path('enquente/<int:id_enquete>/votacao', views.votacao, name='votacao'),
] 