from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .models import Question, Choice, Category
from django.views import generic
from django.urls import reverse
from django.utils import timezone

# Create your views here.
#def index(request):
    #ultimas_perguntas = Question.objects.order_by('-data_publicacao')[:5]
    #contexto = {'ultimas_perguntas': ultimas_perguntas}
    #return render(request, 'main/index.html', contexto)

def detalhes(request, id_enquete):
    pergunta = get_object_or_404(Question, pk=id_enquete)
    category = Category.objects.get(question__id=pergunta.id)
    return render(request, 'main/detalhes.html', {'question': pergunta, 'category': category})

#def resultado(request, id_enquete):
    #pergunta = get_object_or_404(Question, pk=id_enquete)
    #return render(request, 'main/resultado.html', {'pergunta': pergunta})

class IndexView(generic.ListView):
    template_name = "main/index.html" #optional
    context_object_name = 'ultimas_perguntas' #optional
    def get_queryset(self):
        return Question.objects.filter(data_publicacao__lte = timezone.now()).order_by('-data_publicacao')[:5]

# class DetalhesView(generic.DetailView):
#     model = Question #optional
#     template_name = "main/detalhes.html" #optional

#     def get_queryset(self):
#         return Question.objects.filter(data_publicacao__lte = timezone.now())

class ResultadoView(generic.DetailView):
    template_name = "main/resultado.html" #optional
    model = Question #optional

def votacao(request, id_enquete):
    pergunta = get_object_or_404(Question, pk=id_enquete)
    try:
        opcao_selecionada = pergunta.choice_set.get(pk=request.POST['opcao'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'main/detalhes.html',  {
            'pergunta': pergunta,
            'error_message': "Selecione uma opção Válida!"
        })
    else:
        opcao_selecionada.votos += 1
        opcao_selecionada.save()
    return HttpResponseRedirect(reverse('main:resultado', args=(pergunta.id,)))

def categories(request, id_category):
    category = get_object_or_404(Category, pk=id_category)
    return render(request, 'main/categorias.html', {'category': category})