from django.contrib import admin
from .models import Question, Choice, Category

# class OpcaoInLine(admin.StackedInline):
#   model = Choice
#   extra = 2

class OpcaoInLine(admin.TabularInline):
  model = Choice
  extra = 2

class PerguntaAdmin(admin.ModelAdmin):
  fieldsets = [
    (None, {'fields': ['text']}),
    ('Categorias', {'fields': ['category']}),
    ('Informações de datas', {'fields': ['data_publicacao']}),
    ]
  inlines = [OpcaoInLine]
  list_display = ('text', 'id', 'data_publicacao', 'publicada_recentemente')
  list_filter = ['data_publicacao']
  search_fields = ['text']

# Register your models here.
admin.site.register(Question, PerguntaAdmin)
# admin.site.register(Choice)
admin.site.register(Category)