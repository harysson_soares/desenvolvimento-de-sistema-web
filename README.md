# TODO

1. Funcionalidade adicional - classificação das enquetes em categorias;
1. NOVO modelo “Categoria” {título, descrição e data de criação};
1. Uma “categoria” poderá ser associada a várias “enquetes”;
1. Alteração da view de “detalhes” da enquete, exibindo também informações da categoria da enquete, com um link no nome da categoria (que ao ser clicado exibe a lista de enquetes daquela categoria);
1. NOVA view para exibição da lista de enquetes associadas à respectiva categoria (ordenadas alfabeticamente pelo texto), com links para os detalhes de cada enquete e um link para retornar à página inicial da aplicação;
Obs.: os dados e os relacionamentos, para teste da funcionalidade, devem ser inseridos a partir da interface de administração do Django.
